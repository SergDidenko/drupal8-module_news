<?php

namespace Drupal\news_custom_module\Controller;

use Drupal\Core\Controller\ControllerBase;

use Drupal\node\Entity\Node;

use Drupal\node\NodeInterface;

use Drupal\Core\Url;

use Drupal\Core\Link;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class NewsPageController.
 *
 * @package Drupal\news_custom_module\Controller
 */
class NewsPageController extends ControllerBase {

  public function newsList() {
    $perpage = 5;
    $headers = array(
      array(
        'data'      => $this->t('Title'),
        'field'     => 'title',
        'specifier' => 'title'
      ),
      array(
        'data'      => $this->t('Date'),
        'field'     => 'created',
        'specifier' => 'created'
      ),
      array(
        'data'      => $this->t('Status'),
        'specifier' => 'status'
      ),
      array(
        'data'      => $this->t('Subtitle'),
        'specifier' => 'subtitle'
      ),
      array(
        'data'      => $this->t('Description'),
        'specifier' => 'description'
      ),
      array(
        'data' => $this->t('Update'),
      ),
      array(
        'data' => $this->t('Delete'),
      ),
    );
    $query   = \Drupal::entityQuery('node');
    $query->condition('status', NODE_PUBLISHED);
    $query->condition('type', 'newspages');
    $query->tableSort($headers);
    $query->pager($perpage);
    $results = $query->execute();
    $rows    = array();
    foreach ($results as $nid) {
      $news   = Node::load($nid);
      $rows[] = array(
        'data' => array(
          $news->get('title')->getString(),
          \Drupal::service('date.formatter')->format($news->get('created')
                                                          ->getString()),
          $news->get('status')->getString() == NODE_PUBLISHED ? 'published' : '',
          $news->get('field_subtitle')->getValue()[0]['value'],
          $news->get('field_description')->getValue()[0]['value'],
          Link::fromTextAndUrl($this->t('Update'), Url::fromRoute('news_custom_module.news_edit', array('node' => $nid))),
          Link::fromTextAndUrl($this->t('Delete'), Url::fromRoute('news_custom_module.news_delete', array('node' => $nid))),
        )
      );
    }
    $output          = array();
    $output['table'] = array(
      '#theme'  => 'table',
      '#header' => $headers,
      '#rows'   => $rows
    );
    $output['pager'] = array(
      '#type' => 'pager'
    );
    return $output;
  }

  public function deleteNews(NodeInterface $node = NULL) {
    if (NULL != $node && $node->get('type')->getString() == "newspages") {
      $node->delete();
      return $this->redirect('news_custom_module.news');
    }
    throw new NotFoundHttpException();
  }
}

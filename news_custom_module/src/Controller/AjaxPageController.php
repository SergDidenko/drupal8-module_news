<?php

namespace Drupal\news_custom_module\Controller;

use Drupal\Core\Ajax\AjaxResponse;

use Drupal\Core\Ajax\HtmlCommand;

use Drupal\Core\Controller\ControllerBase;

use Drupal\node\Entity\Node;

class AjaxPageController extends ControllerBase {
  public function showLastNews() {
    $month   = $_POST['month'];
    $year    = $_POST['year'];
    $nids    = \Drupal::database()->select('node_field_data', 'n')
                      ->fields('n', array('nid'))
                      ->orderBy('created', 'DESC')
                      ->condition('type', 'newspages')
                      ->where('MONTH(FROM_UNIXTIME(n.created)) = :month', array('month' => $month))
                      ->where('YEAR(FROM_UNIXTIME(n.created)) = :year', array('year' => $year))
                      ->range(0, 2)
                      ->execute()
                      ->fetchCol('nid');
    $results = Node::loadMultiple($nids);
    if (!is_null($results)) {
      $news = array();
      foreach ($results as $res) {
        $nid                       = $res->get('nid')->getString();
        $news[$nid]['title']       = $res->get('title')->getString();
        $news[$nid]['subtitle']    = $res->get('field_subtitle')
                                         ->getValue()[0]['value'];
        $news[$nid]['description'] = $res->get('field_description')
                                         ->getValue()[0]['value'];
      }
      $response = new AjaxResponse($news);
//      $response->addCommand(new HtmlCommand('.ajax-news', 'sdbdb'));
      return $response;
    }
  }
}
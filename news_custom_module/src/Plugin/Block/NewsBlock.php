<?php
/**
 * @file
 * Contains \Drupal\news_custom_module\Plugin\Block\NewsBlock.
 */

namespace Drupal\news_custom_module\Plugin\Block;

use Drupal\Core\Block\BlockBase;

use Drupal\Core\Form\FormStateInterface;

use Drupal\node\Entity\Node;

/**
 * @Block(
 *   id = "news_block",
 *   admin_label = @Translation("News Block")
 * )
 */
class NewsBlock extends BlockBase {

  public function build() {
    $config = $this->getConfiguration();
    $num    = $config['number'];
    $query  = \Drupal::entityQuery('node');
    $query->condition('status', 1);
    $query->condition('type', 'newspages');
    $query->sort('created', 'DESC');
    $query->range(0, $num);
    $news_items = $query->execute();
    $news       = array();
    foreach ($news_items as $nid) {
      $news_item                 = Node::load($nid);
      $news[$nid]['title']       = $news_item->get('title')->getString();
      $news[$nid]['subtitle']    = $news_item->get('field_subtitle')
                                             ->getValue()[0]['value'];
      $news[$nid]['description'] = $news_item->get('field_description')
                                             ->getValue()[0]['value'];
    }
    $block = array(
      '#theme' => 'news_block',
      '#news'  => $news,
    );
    return $block;
  }

  public function defaultConfiguration() {
    return array(
      'number' => 3,
    );
  }

  public function blockForm($form, FormStateInterface $form_state) {
    $form           = parent::blockForm($form, $form_state);
    $config         = $this->getConfiguration();
    $form['number'] = array(
      '#type'          => 'number',
      '#title'         => 'The number of news should be shown',
      '#default_value' => $config['number'],
    );
    return $form;
  }

  public function blockValidate($form, FormStateInterface $form_state) {
    $num = $form_state->getValue('number');
    if (!is_numeric($num) || $num < 1) {
      $form_state->setErrorByName('number', $this->t('It should be contain only integer and should be more or equal 1.'));
    }
  }

  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['number'] = $form_state->getValue('number');
  }
}
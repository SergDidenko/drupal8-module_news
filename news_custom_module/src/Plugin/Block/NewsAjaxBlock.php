<?php

namespace Drupal\news_custom_module\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * @Block(
 *   id = "ajax-block",
 *   admin_label = @Translation("Ajax Block")
 * )
 */
class NewsAjaxBlock extends BlockBase {
  public function build() {
    $block = array(
      '#theme' => 'ajax_block',
    );
    return $block;
  }
}

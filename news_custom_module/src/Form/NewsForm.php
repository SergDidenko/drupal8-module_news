<?php

namespace Drupal\news_custom_module\Form;

use Drupal\Core\Form\FormBase;

use Drupal\Core\Form\FormStateInterface;

use Drupal\node\NodeInterface;

use Drupal\node\Entity\Node;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class NewsForm extends FormBase {
  public function getFormId() {
    return 'news_form_controller';
  }

  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = NULL) {
    if (isset($node) && $node->get('type')->getString() != "newspages") {
      throw new NotFoundHttpException();
    }else {
      $form['title']       = array(
        '#title'         => $this->t('Title'),
        '#description'   => $this->t('The title of news'),
        '#type'          => 'textfield',
        '#default_value' => isset($node) ? $node->get('title')
                                                ->getString() : '',
        '#required'      => TRUE
      );
      $form['status']      = array(
        '#title'         => $this->t('Status'),
        '#description'   => $this->t('The status of news'),
        '#type'          => 'select',
        '#default_value' => isset($node) ? $node->get('status')
                                                ->getString() : '',
        '#options'       => array(
          1 => $this->t('Published'),
          0 => $this->t('Unpublished')
        ),
        '#required'      => TRUE
      );
      $form['subtitle']    = array(
        '#title'         => $this->t('Subtitle'),
        '#description'   => $this->t('The subtitle of news'),
        '#type'          => 'textfield',
        '#default_value' => isset($node) ? $node->get('field_subtitle')
                                                ->getValue()[0]['value'] : '',
        '#required'      => TRUE
      );
      $form['description'] = array(
        '#title'         => $this->t('Description'),
        '#description'   => $this->t('The description of news'),
        '#type'          => 'textarea',
        '#default_value' => isset($node) ? $node->get('field_description')
                                                ->getValue()[0]['value'] : '',
        '#required'      => TRUE
      );
      $form['submit']      = array(
        '#type'        => 'submit',
        '#value'       => isset($node) ? 'Update' : 'Create',
        '#button_type' => 'primary'
      );
      if (isset($node)) {
        $form['nid'] = array(
          '#type'  => 'value',
          '#value' => $node->get('nid')->getString(),
        );
      }
      return $form;
    }

  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $title       = $form_state->getValue('title');
    $status      = $form_state->getValue('status');
    $description = $form_state->getValue('description');
    $subtitle    = $form_state->getValue('subtitle');
    if (!empty($title) && !preg_match("/^[a-zA-Z0-9\s]+$/", $title)) {
      $form_state->setErrorByName('title', $this->t('It should be included only letters'));
    }
    if (!empty($subtitle) && !preg_match("/^[\w\s]+$/", $subtitle)) {
      $form_state->setErrorByName('subtitle', t('It should be included only letters and digits'));
    }
    if (!empty($description) && !preg_match("/^[a-zA-Z\s]+$/", $description)) {
      $form_state->setErrorByName('description', t('It should be included only letters'));
    }
  }

  function submitForm(array &$form, FormStateInterface $form_state) {
    if (NULL != $form_state->getValue('nid')) {
      $nid  = $form_state->getValue('nid');
      $node = Node::load($nid);
      $node->setTitle($form_state->getValue('title'));
      $node->set('status', $form_state->getValue('status'));
      $node->set('field_description', $form_state->getValue('description'));
      $node->set('field_subtitle', $form_state->getValue('subtitle'));
      $node->save();
    }
    else {
      $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $node     = Node::create(array(
        'type'              => 'newspages',
        'title'             => $form_state->getValue('title'),
        'langcode'          => $language,
        'uid'               => \Drupal::currentUser()->id(),
        'status'            => $form_state->getValue('status'),
        'field_description' => array($form_state->getValue('description')),
        'field_subtitle'    => array($form_state->getValue('subtitle'))
      ));
      $node->save();
    }
    $form_state->setRedirect('news_custom_module.news');
    return;
  }
}


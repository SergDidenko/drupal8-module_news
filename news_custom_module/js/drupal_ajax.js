(function ($) {
    Drupal.behaviors.newsAjax = {
        attach: function (context, settings) {
            var datePicker = $(context).find('.date-picker').once().datepicker(
                {
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    dateFormat: 'MM yy',
                    constrainInput: true,
                    showOn: 'button',
                    buttonText: 'Select...'
                }
            );
            datePicker.focus(
                function () {
                    var thisCalendar = $(this);
                    $('.ui-datepicker-calendar').detach();
                    $('.ui-datepicker-close').click(
                        function () {
                            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                            thisCalendar.datepicker('setDate', new Date(year, month, 1));
                            $.ajax({
                                type: "POST",
                                url: '/ajax',
                                data: {'month': +month + 1, 'year': year},
                                success: function (response) {
                                    $('.ajax-news').empty();
                                    for (var r in response) {
                                        $('.ajax-news').prepend('<div class="ajax-news-content ajax-news-content-' + r + '"></div>');
                                        $('.ajax-news-content-' + r).prepend('<h4 class="ajax-news-item">Description: ' + response[r].description + '</h4><br>');
                                        $('.ajax-news-content-' + r).prepend('<h4 class="ajax-news-item">Subtitle: ' + response[r].subtitle + '</h4><br>');
                                        $('.ajax-news-content-' + r).prepend('<h4 class="ajax-news-item">Title: ' + response[r].title + '</h4><br>');
                                    }
                                }
                            });
                        }
                    );
                }
            );
        }
    };
})(jQuery);